RHFC Connect will allow Riverside Health Fitness Center (RHFC) members to have access to the facility at their fingertips! The app will provide Access to the following:

• Keep up to date on all things RHFC.

• Schedule gym appointments at home or at work.

• Full access to your membership account.

• Quick access to Land and Aquatic Group Exercise schedules.

• Easy access to our website and facility hours/contact information.

This is the popular product of MiGym.

APK file size of this app is 9.3M, and we suggest you use the Wi-fi connection when downloading to save your 3G data. RHFC Connect works with Android "4.4" and higher version, so please check your system before you install it. 
Download link: https://apkfun.com/RHFC-Connect.html

The latest update of this APK file is on Feb 8, 2022. If you have any trouble with RHFC Connect, feel free to go MiGym website and contact with the app's developers. We hope you enjoy this app and share it with your friends on Facebook, Google+ or Twitter.

Thank you and enjoy RHFC Connect now!
 
